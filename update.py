#!/usr/bin/env python3

"""Append entries to the Cannabis.csv file using the proper
formatting."""

from pathlib import Path
from pprint import pformat
import traceback

import pymsgbox
# from IPython.core.debugger import set_trace

from modules import helpers

"""Add:
    if "Cancel", close program
    Logging
    Capital_Case + Acronyms (e.g. OG)
    while_not_blank(Action: callable, *args) -> str:
        check for cancel here too
"""

# # Module Constants # #

# Path
Cwd = Path(".").resolve()
CsvPath = Cwd / "data/csv"
CannabisPath = Cwd / "Cannabis.csv"
BackupPath = CsvPath / "Cannabis_bak.csv"

# CSV
Title = "Cannabis Tracker"
Exception_Title = "Exception!"
Field_Names = ("Strain",
               "Type",
               "Dominance",
               "Weight (g)",
               "THC (%)",
               "CBD (%)",
               "Total Cannabinoids (%)",
               "Seller",
               "Rating")


def while_not_float(Action: callable, *args) -> float:
    """While the result of 'Action' is not a float, reapeat
    'Action'.
    '*args' is used as arguments for 'Action'"""
    while True:
        result = Action(*args)
        try:
            result = float(result)
            break
        except (ValueError, TypeError):
            # ValueError: if result is non-numeric
            # TypeError: if result is NoneType
            pymsgbox.alert("Error! Please enter a number to continue!")

    return result


def user_confirm(Entries: dict) -> bool:
    """Uses PyMsgBox to print a dict of new entries along with
    confirmation buttons to confirm or cancel adding the new
    entries."""
    # Format using 'pformat' from 'pprint'
    formatted_entries = pformat(Entries, sort_dicts=False)
    msg = f"Confirm entry or entries below:\n\n{formatted_entries}"
    answer = pymsgbox.confirm(msg, buttons=("Confirm", "Discard"),
                              title=Title)
    if answer == "Confirm":
        return True
    else:
        return False


def has_more_entries() -> bool:
    """Uses PyMsgBox to ask user if they would like to continue
    with data entry.
    Returns True or False based on their answer."""
    msg = "Do you have more to enter?"
    answer = pymsgbox.confirm(msg, title=Title, buttons=("Yes", "No"))
    if answer == "Yes":
        return True
    else:
        return False


def backup_notify(Entries: (dict)) -> None:
    """Backup 'Entries' and notify user.
    'Entries' expects a dict of dicts.
    If an IOError is raised, notifies user to write down 'Entries'
    before exiting."""
    # Backup entry
    try:
        helpers.dict_to_csv(list(Entries.values()),
                            BackupPath, 'a')
    except IOError:
        pymsgbox.alert(traceback.format_exc(), title=Exception_Title)
        pymsgbox.alert(f"Failed to save entries to file: {BackupPath}",
                       title=Title)

        # Format using 'pformat' from 'pprint'
        formatted_entries = pformat(Entries, sort_dicts=False)
        msg = ("Below entries will be lost if you don't "
               f"write them down:\n{formatted_entries}")
        pymsgbox.alert(msg, title=Title)

        exit(-2)  # IOError, failed to save

    msg = ("Your entries from this session have been added to:\n\n"
           f"{BackupPath}\n\n"
           "Just in case...")
    pymsgbox.alert(msg, title=Title)

    return None


def get_entry() -> None:
    """Uses PyMsgBox to get specified data from user in proper
    order.
    Returns an dict containing the new entry."""
    while True:
        # Get entry fields
        Strain = pymsgbox.prompt("Enter the Strain:", title=Title)
        msg = ("Enter the Type:\n\n"
               "(e.g. Wax, Shatter, Distillate, Flower, etc.)\n")
        Type = pymsgbox.prompt(msg, title=Title)  # 'Form'
        Dominance = pymsgbox.prompt("Enter the Dominance:\n\n"
                                    "(e.g. Sativa, Indica, Hybrid, CBD)\n",
                                    title=Title)
        Weight = while_not_float(pymsgbox.prompt, "Enter the Weight (g):",
                                 Title)
        msg = "Enter the THC content as a percent (%):"
        Percent_Thc = while_not_float(pymsgbox.prompt, msg, Title)
        msg = "Enter CBD content as a percent (%):"
        Percent_Cbd = while_not_float(pymsgbox.prompt, msg, Title)
        msg= "Enter Total Cannabinoid content as a percent (%):"
        Total_Cannabinoids_Percent = while_not_float(pymsgbox.prompt,
                                                     msg,
                                                     Title)
        Seller = pymsgbox.prompt("Enter the Seller:", title=Title)
        Rating = pymsgbox.prompt("Enter your Rating:\n"
                                 "(e.g. -, --, ..., +, ++, ...)\n",
                                 title=Title)

        # Build entry
        new_entry = {"Strain": Strain,
                     "Type": Type,
                     "Dominance": Dominance,
                     "Weight (g)": Weight,
                     "THC (%)": Percent_Thc,
                     "CBD (%)": Percent_Cbd,
                     "Total Cannabinoids (%)": Total_Cannabinoids_Percent,
                     "Seller": Seller,
                     "Rating": Rating}

        # Confirm new entry
        if user_confirm(new_entry):
            break
        else:
            answer = pymsgbox.confirm("Quit?", title=Title,
                                      buttons=("Yes", "No"))
            if not answer == "No":
                backup_notify({"Entry1": new_entry})
                # 'backup_notify' expects a dict of dicts
                return None

    return new_entry


def main() -> None:
    # Create CSV tables, if they don't already exist
    if not CannabisPath.exists():
        helpers.save_csv(Field_Names, CannabisPath)
    if not BackupPath.exists():
        helpers.save_csv(Field_Names, BackupPath)

    datatable = helpers.load_csv(CannabisPath)
    new_entries = dict()

    while True:
        try:
            n = 0  # Entry number
            while True:
                n += 1
                new_entry = get_entry()
                if new_entry is None:
                    exit(0)  # User quit
                new_entries.update({f"Entry{n}": new_entry})
                if not has_more_entries():
                    break
            # Check if there were multiple entries
            if n > 1:
                # Confirm multiple entries
                if user_confirm(new_entries):
                    break
                else:
                    backup_notify(new_entries)
                    new_entries.clear()  # Prepare dict for next iteration
            else:  # If only 1 entry, no need to confirm again
                break
        except Exception:
            pymsgbox.alert(traceback.format_exc(), title=Exception_Title)
            backup_notify(new_entries)
            msg = ("Your entries from this session have been added to:\n\n"
                   f"{BackupPath}\n\n"
                   "Just in case...")
            pymsgbox.alert(msg, title=Title)
            exit(-1)  # Catch-all

    helpers.dict_to_csv(list(new_entries.values()), CannabisPath, 'a')

    return None


if __name__ == "__main__":
    main()
